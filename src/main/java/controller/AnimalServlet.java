/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AnimalDAO;
import model.Animal;

/**
 * @email Ramesh Fadatare
 */
@WebServlet("/registered")
public class AnimalServlet
        extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private AnimalDAO animalDao;

    public void init() {
        animalDao = new AnimalDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String spieces = request.getParameter("species");
        String genus = request.getParameter("genus");
        String family = request.getParameter("family");
        String clas = request.getParameter("clas");
      

        Animal animal = new Animal();
        animal.setSpieces(spieces);
        animal.setGenus(genus);
        animal.setFamily(family);
        animal.setClas(clas);

        try {
            animalDao.registerAnimal(animal);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        response.sendRedirect("registered.jsp");
    }
}
/**
 *
 * @author Tomas
 */
