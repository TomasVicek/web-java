/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author Tomas
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import model.Animal;

public class AnimalDAO {

    private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence
            .createEntityManagerFactory("Animals");
    
    
    public ArrayList<Animal> readAnimals() throws ClassNotFoundException {
        String READ_ANIMALS = "SELECT * FROM animals";

        ResultSet result = null;
        ArrayList<Animal> animals = new ArrayList<Animal>();

    
        
        Class.forName("com.mysql.jdbc.Driver");

        try {
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/animals?zeroDateTimeBehavior=CONVERT_TO_NULL", "root", "1234");
            Statement preparedStatement = connection.createStatement();
            result = preparedStatement.executeQuery(READ_ANIMALS);
            while (result.next()) {
                
                Animal animal = new Animal();
                animal.setSpieces(result.getString("species"));
                animal.setGenus(result.getString("genus"));
                animal.setFamily(result.getString("family"));
                animal.setClas(result.getString("class"));
                
                animals.add(animal);
            }

        } catch (SQLException e) {
            // process sql exception
           // printSQLException(e);
        }
    return animals;}

    public void registerAnimal(Animal animal) throws ClassNotFoundException {
            EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        // Used to issue transactions on the EntityManager
        EntityTransaction et = null;


        try  {et = em.getTransaction();
            et.begin();
            em.persist(animal);
            et.commit();
            
            
 } catch (Exception ex) {
            // If there is an exception rollback changes
            if (et != null) {
                et.rollback();
            }
            ex.printStackTrace();
        } finally {
            // Close EntityManager
            em.close();
        }
        
    }

    /*private void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }*/
}
